extends LinkButton

export(String) var scene_to_load


func _on_New_Game_button_up():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))


func _on_back_to_main_menu_button_up():
	get_tree().change_scene(str("res://Scenes/" + "MarginContainer" + ".tscn"))
