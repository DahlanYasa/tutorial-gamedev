extends KinematicBody


export var speed = 10
export var run_speed = 20
export var crouch_speed = 5
export var acceleration = 5
export var gravity = 0.58
export var jump_power = 30
export var mouse_sensitivity = 0.3

onready var head = $Head
onready var camera = $Head/Camera

var velocity = Vector3()
var camera_x_rotation = 0

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		head.rotate_y(deg2rad(-event.relative.x * mouse_sensitivity))
		
		var x_delta = event.relative.y * mouse_sensitivity
		if camera_x_rotation + x_delta > -90 and camera_x_rotation + x_delta < 90:
			camera.rotate_x(deg2rad(-x_delta))
			camera_x_rotation += x_delta

func _process(_delta):
	if Input.is_action_just_pressed("ui_cancel"):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		
func _physics_process(delta):
	var head_basis = head.get_global_transform().basis
	
	var forward = Input.is_action_pressed("movement_forward")
	var backward = Input.is_action_pressed("movement_backward")
	var left = Input.is_action_pressed("movement_left")
	var right = Input.is_action_pressed("movement_right")
	var sprint = Input.is_action_pressed("sprint")
	
	var movement_vector = Vector3()
	if forward:
		movement_vector -= head_basis.z
	if backward:  
		movement_vector += head_basis.z
	if left:
		movement_vector -= head_basis.x
	if right:
		movement_vector += head_basis.x
		
	movement_vector = movement_vector.normalized()
	
	if sprint:
		velocity = velocity.linear_interpolate(movement_vector * run_speed, acceleration * delta)
	else:
		velocity = velocity.linear_interpolate(movement_vector * speed, acceleration * delta)
	velocity.y -= gravity
	
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y += jump_power
		
	velocity = move_and_slide(velocity, Vector3.UP)
	
	
	
