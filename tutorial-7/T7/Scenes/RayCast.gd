extends RayCast


var current_collider
var held
onready var hold_position = $"../Position3D"

func _ready():
	pass # Replace with function body.


func _process(delta):
	var collider = get_collider()
	
	if !collider:
		return
	
	if is_colliding() and collider is Interactable:
		if Input.is_action_just_pressed("interact"):
			collider.interact()
			
	if is_colliding() and collider is Holdable:
		if Input.is_action_just_pressed("interact"):
			held = collider
			
	
