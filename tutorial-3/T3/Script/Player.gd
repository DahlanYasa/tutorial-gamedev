extends KinematicBody2D

export (int) var speed = 400
var GRAVITY = 1200
const UP = Vector2(0,-1)
var velocity = Vector2()

#jump
var jump_speed = -600
var max_jumps = 2
var jump_count = 0

#dash
var dashDirection = Vector2(1, 0)
var canDash = false
var dashing = false

func get_input():
	velocity.x = 0
	if Input.is_action_just_pressed('up') && jump_count < max_jumps:
		velocity.y = jump_speed
		jump_count += 1
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
		
	if Input.is_action_just_pressed("ui_dash") and canDash:
		velocity = dashDirection.normalized() * 10000
		canDash = false
		dashing = true
		yield(get_tree().create_timer(50), "timeout")
		dashing = false


func _physics_process(delta):
	dash()
	
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
	if is_on_floor():
		jump_count = 0
		jump_speed = -600
		canDash = true
		
	if jump_count == 1:
		jump_speed = -400
	
	
	
func dash():
	if Input.is_action_pressed("ui_right"):
		dashDirection = Vector2(1, 0)
	if Input.is_action_pressed("ui_left"):
		dashDirection = Vector2(-1, 0)
	
	
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
