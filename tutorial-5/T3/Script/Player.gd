extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -400

const UP = Vector2(0,-1)
var velocity = Vector2()
var max_jumps = 2
var jump_count = 0
onready var animator = self.get_node("AnimatedSprite")


func get_input():
	velocity.x = 0
	if Input.is_action_just_pressed('up') && jump_count < max_jumps:
		velocity.y = jump_speed
		jump_count += 1
	
		
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
		

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
	if is_on_floor():
		jump_count = 0
		jump_speed = -400
		
	if jump_count == 1:
		jump_speed = -400
		
	if velocity.y != 0:
		animator.play("jump")
	elif velocity.x != 0:
		animator.play("jalan_kanan")
		if velocity.x > 0:
			animator.flip_h = false
		else:
			animator.flip_h = true
	else:
		animator.play("diri_kanan")
	print(jump_count)
	if Input.is_action_pressed('down'):
		animator.play("nunduk")
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
